function Set-IpAdress-Env () {
    $adapter = (Get-NetAdapter -Physical)
    $adress = (Get-NetIPAddress -AddressFamily IPv4 -InterfaceIndex $adapter[0].ifIndex)
    $strIpAdress = 'REACT_APP_API_URL="' + $adress.IPAddress + ':3000"'
    $strIpAdress > app-env.env
    $strIpAdress = 'REACT_APP_API_PROXY_URL="' + $adress.IPAddress + ':4040"'
    $strIpAdress >> app-env.env
}

while($true) {
    Write-Host
    Write-Host "Menu K8s-Gitlab docker-compose for develop" -BackgroundColor White -ForegroundColor Red
    Write-Host

    Write-Host "1. Run K8s-Gitlab in currnect session" -ForegroundColor Green
    Write-Host "2. Run K8s-Gitlab in background session" -ForegroundColor Green
    Write-Host "3. Stop K8s-Gitlab in background session" -ForegroundColor Green
    Write-Host "4. Get status K8s-Gitlab in background session" -ForegroundColor Green
    Write-Host "5. Get configured port" -ForegroundColor Green
    Write-Host "6. Clean all" -ForegroundColor Green
    Write-Host "7. Exit" -ForegroundColor Green
    Write-Host

    $choice = Read-Host "Select the menu item"

    Switch($choice){
        1{Set-IpAdress-Env
            docker-compose up --build}
        2{Set-IpAdress-Env
            docker-compose up -d --build}
        3{docker-compose down}
        4{docker-compose ps}
        5{Write-Host "Api use port 4040, App use port 3000"
            Read-Host "Press 'Entrer' for continue..."}
        6{$str = (Get-Location).Path
            $str = $str.Split("\")
            $str = $str[$str.Count - 1]
            $str = $str.ToLower()
            docker container rm k8s-gitlab-api --force
            docker container rm k8s-gitlab-app --force
            docker volume rm ($str + "_api_data")
            docker network rm ($str + "_k8sgitlab")}
        7{Write-Host "Exit"; exit}
        default {Write-Host "Wrong choice, try again." -ForegroundColor Red
        Read-Host "Press 'Entrer' for continue..."}
    }
}4