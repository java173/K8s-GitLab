import { useState, useEffect } from "react";

const Log = (props) => {
    const [logText, setLogText] = useState(props.children);
    const loadLogs = () => {
        fetch(`${props.config.apiUrl}/log`)
        .then((response) => response.json())
        .then((data) => setLogText(data));
    };

    useEffect(loadLogs);

    const logging = () => {
        // send request to API to log current user
        fetch(`${props.config.apiUrl}/log`, { method: "POST" })
          .then(props.callback)
          .then(loadLogs);
      };

    return (
        <>
          <button onClick={logging}>create a log</button>
          <div className="log">
            {("" || logText).split("\n").map((l, i) => (
              <p key={i}>{l}</p>
            ))}
          </div>
        </>
      );
    };

    export default Log;
    