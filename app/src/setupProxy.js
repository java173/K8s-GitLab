const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/log',
    createProxyMiddleware({
      target: process.env.REACT_APP_API_PROXY_URL || "http://localhost:4040",
      changeOrigin: true,
    })
  );
};