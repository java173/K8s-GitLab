
# Kubernetes deployment

For a detailed review of the application, just clone the project to your local folder and go to the newly created folder

```
git clone https://gitlab.com/java173/K8s-GitLab.git
cd K8s-Gitlab
```
This is the simplest application that meets the following criteria.
Uses a modern web framework(React.js) requiring code compilation.
Requires passing a parameter(API_URL) at the build time.
Contains server-side code.
Includes a Front End and API for emulating microservice architecture.
Has some persistent state.
Contains tests.
We aimed to keep the application as simple as possible, so I deliberately didn't include things like this:
authentication and authorization;
logs and telemetry;
error handling;
interaction with database;
any meaningful test coverage.
Inside the newly cloned repository, there will be files and directories:

```
api                 // Directory with API files
  /data/log         // We will save data here
  /package.json     // API NPM manifest
  /server.js        // API code, does not require installing other packages
app                 // Directory with APP files
  /public/.         // React standard static files
  /src
    /components
      /Log.jsx      // This component sends API requests
    /App.js         // Main APP application component
    /App.test.js    // Tests, we must have some tests
    /config.js      // Loads an API URL from an environment variable
    /index.js       // React application root
  /package.json     // APP NPM manifest
  /package-lock.json  // Worked with these versions of packages
  ```
  You need run `docker-compose up` for local deploy app and api. Preveosly run you must change environment `REACT_APP_API_URL` and `REACT_APP_API_PROXY_URL`.

To start docker-compose, you must install dokker and dokker-compose, the user must have rights to run docker. And in the current directory you should have a clone of the repositories "K8s-GitLab"
To start on Windows, you need to run:
```Set-ExecutionPolicy -Scope Process -ExecutionPolicy RemoteSigned –Force```
This is to allow the script to run in this powershell session. 
Follow if you need a permanent alow rule
```Set-ExecutionPolicy -ExecutionPolicy RemoteSigned```
Next you run init.ps1

To start on Linux or MacOS, you need to run init.sh