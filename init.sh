#!/bin/bash
function exitmenu() {
	echo "Exit"
    exit 0;
}

function set-IpAdress-env() {
    for iface in $(find /sys/class/net/ -type l ! -lname '*/devices/virtual/net/*' -printf '%f '); 
        do
            addr=$(ip addr | grep enp | grep 'inet' | cut -c10-26)
            IFS="/" read -ra result_array <<< "$addr"
            str='REACT_APP_API_URL="'${result_array[0]}':3000"'
            echo $str > app-env.env
            str='REACT_APP_API_PROXY_UR="'${result_array[0]}':4040"'
            echo $str >> app-env.env
        done 
}

function up() {
    set-IpAdress-env
    docker-compose up --build
}

function up-background() {
    set-IpAdress-env
    docker-compose up -d --build
}

function down-background() {
    docker-compose down
}

function ls-background() {
    docker-compose ps
}

function get-configuration() {
    echo "Api use port 4040, App use port 3000"
    echo "Press 'Entrer' for continue..."
    read
}

function clear-all() {
    str=$(pwd)
    IFS="/" read -ra result_array <<< "$str"
    count=${#result_array[@]}
    str=$(echo "${result_array[$count-1]}" | tr '[:upper:]' '[:lower:]')
    docker container rm k8s-gitlab-api --force
    docker container rm k8s-gitlab-app --force
    docker volume rm "${str}_api_data"
    docker network rm "${str}_k8sgitlab"
}

##
# Color  Variables
##
green='\e[32m'
blue='\e[34m'
clear='\e[0m'

##
# Color Functions
##

ColorGreen(){
	echo -ne $green$1$clear
}
ColorBlue(){
	echo -ne $blue$1$clear
}
menu(){
echo -ne "
Menu K8s-Gitlab docker-compose for develop
$(ColorGreen '1)') Run K8s-Gitlab in currnect session
$(ColorGreen '2)') Run K8s-Gitlab in background session
$(ColorGreen '3)') Stop K8s-Gitlab in background session
$(ColorGreen '4)') Get status K8s-Gitlab in background session
$(ColorGreen '5)') Get configured port
$(ColorGreen '6)') Clean all
$(ColorGreen '7)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
	        1) up ; menu ;;
	        2) up-background ; menu ;;
	        3) down-background ; menu ;;
	        4) ls-background ; menu ;;
            5) get-configuration ; menu ;;
            6) clear-all ; menu ;;
			7) exitmenu ;;
			*) echo -e $red"Wrong option."$clear; menu;;
        esac
}


menu